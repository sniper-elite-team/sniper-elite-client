# Sniper Elite Steps

## Step 1
### Get Arena from Server

We have now a server that, after a certain amount of players are 
in the session, broadcast (over the session), some information. One of
those infos are the Arena that the player will play.
The client need to render that map.

### ToDo
1. Think about threading
    * We know that we need 2 threads to have a full duplex communication. 
      How can we achieve that?
      
1. Receive information
    
   * Where are we going to get that info? 
    
   * Do we have commands? As the server, the client might need the 
      help of commands (enum, interface).

   * How will we render the Arena after we get the info? Who is the 
      responsible for the screen?
     
1. Display the Arena
    * Think about a strategy to help you as a back-end developer. Do we 
      need pictures right now?
      

### Solutions
#### Branch: receive_arena

1. Think about threading
   * By using the Main thread for the RequestHandler && using the SimpleGraphics 
     auto generated threads for the ResponseHandler.

1. Receive information
   * We receive information from the server through our ResponseHandler.   

   * Yes we need to have commands in order to interpret the info from our server
     and know what to tell our screen to execute. 

   * Our commands will know what to tell the screen to execute.
   
1. Display the Arena

   * lets go step by step, we use rectangles for now.
