package org.snipereliteteam.snipereliteclient.game_object;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.snipereliteteam.snipereliteclient.screen.Vector2D;

public class GameObject {

    private Picture image;

    private Position position;

    private final int id;


    public GameObject(Picture image, Position position, int id) {
        this.image = image;
        this.position = position;
        this.id = id;
    }


    public void translate(Vector2D vector2D) {
        position.translate(vector2D);
        image.translate(vector2D.getX(), vector2D.getY());
    }


    public Picture getImage() {
        return image;
    }


    public void setImage(Picture image) {
        this.image = image;
    }


    public Position getPosition() {
        return position;
    }


    public void setPosition(Position position) {
        this.position = position;
    }


    public int getId() {
        return id;
    }

}
