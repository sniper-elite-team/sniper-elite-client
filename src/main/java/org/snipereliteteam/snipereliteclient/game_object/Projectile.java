package org.snipereliteteam.snipereliteclient.game_object;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Projectile extends GameObject {

    private final String orientation;


    public Projectile(Picture image, Position position, int id, String orientation) {
        super(image, position, id);
        this.orientation = orientation;
    }


    public String getOrientation() {
        return orientation;
    }

}
