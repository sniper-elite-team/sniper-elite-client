package org.snipereliteteam.snipereliteclient.game_object;

import org.snipereliteteam.snipereliteclient.screen.Vector2D;

public class Position {

    private int x;
    private int y;


    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public void translate(Vector2D vector2D) {
        x += vector2D.getX();
        y += vector2D.getY();
    }


    public int getX() {
        return x;
    }


    public void setX(int x) {
        this.x = x;
    }


    public int getY() {
        return y;
    }


    public void setY(int y) {
        this.y = y;
    }

}
