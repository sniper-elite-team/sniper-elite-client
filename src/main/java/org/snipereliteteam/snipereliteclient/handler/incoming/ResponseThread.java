package org.snipereliteteam.snipereliteclient.handler.incoming;

import org.snipereliteteam.snipereliteclient.handler.incoming.commands.ResponseCommand;
import org.snipereliteteam.snipereliteclient.handler.incoming.commands.ResponseCommandsEnum;

public class ResponseThread implements Runnable {

    private final ResponseHandler responseHandler;


    public ResponseThread(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
    }


    @Override
    public void run() {

        while (!responseHandler.getSocket().isClosed()) {

            synchronized (responseHandler.getResponses()) {
                if (responseHandler.getResponses().isEmpty()) {
                    try {
                        responseHandler.getResponses().wait();
                    } catch (InterruptedException e) {
                        // Nothing to handle
                    }
                }
            }

            String response = responseHandler.getResponses().poll();

            if (response == null) {
                continue;
            }

            String command = response.substring(0, response.indexOf("#"));
            String arguments = response.substring(response.indexOf("#") + 1);

            ResponseCommand responseCommand;

            try {
                responseCommand = ResponseCommandsEnum.valueOf(command).getCommand();
            } catch (IllegalArgumentException e) {
                responseCommand = ResponseCommandsEnum.INVALID.getCommand();
            }
            responseCommand.execute(arguments);

        }
    }

}
