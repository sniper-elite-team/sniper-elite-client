package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class ArenaCommand extends AbstractCommand {

    @Override
    public void execute(String arguments) {
        Picture background = new Picture(0, 0, arguments + ".png");
        screen.buildArena(background);
    }

}
