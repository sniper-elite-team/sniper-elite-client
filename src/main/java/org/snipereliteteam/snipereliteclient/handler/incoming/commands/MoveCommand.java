package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

import org.snipereliteteam.snipereliteclient.game_object.Position;

public class MoveCommand extends AbstractCommand {

    @Override
    public void execute(String arguments) {

        String[] values = arguments.split("#");

        int playerId = Integer.parseInt(values[0]);
        int x = Integer.parseInt(values[1].substring(0, values[1].indexOf(",")));
        int y = Integer.parseInt(values[1].substring(values[1].indexOf(",") + 1));

        String orientation = values[2];

        Position position = new Position(x, y);

        screen.movePlayers(playerId, position, orientation);
    }

}
