package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

public class IdCommand extends AbstractCommand {

    @Override
    public void execute(String arguments) {
        screen.setMyPlayerId(Integer.parseInt(arguments));
    }

}
