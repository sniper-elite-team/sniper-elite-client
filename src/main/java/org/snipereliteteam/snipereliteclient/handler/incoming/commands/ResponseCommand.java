package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

import org.snipereliteteam.snipereliteclient.screen.Screen;

public interface ResponseCommand {

    void execute(String arguments);

    void setScreen(Screen screen);

}
