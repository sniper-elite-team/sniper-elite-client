package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

public class ShotRemoveCommand extends AbstractCommand {

    @Override
    public void execute(String arguments) {
        int projectileId = Integer.parseInt(arguments);
        screen.removeProjectile(projectileId);
    }

}
