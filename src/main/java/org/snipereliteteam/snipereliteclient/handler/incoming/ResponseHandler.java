package org.snipereliteteam.snipereliteclient.handler.incoming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ResponseHandler {

    private final Socket socket;

    private final Queue<String> responses = new ConcurrentLinkedQueue<>();


    public ResponseHandler(Socket socket) {
        this.socket = socket;
    }


    public void startListening() {

        ExecutorService service = Executors.newSingleThreadExecutor();
        service.execute(new ResponseThread(this));

        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
            closeSocket();
            System.exit(0);
        }

        while (!socket.isClosed()) {
            try {

                String response = in.readLine();
                synchronized (responses) {
                    responses.offer(response);
                    responses.notifyAll();
                }

            } catch (IOException e) {
                System.err.println("You have been disconnected");
                closeSocket();
                System.exit(0);
            }
        }
    }


    private void closeSocket() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Queue<String> getResponses() {
        return responses;
    }


    public Socket getSocket() {
        return socket;
    }

}
