package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

public class KillCommand extends AbstractCommand {

    @Override
    public void execute(String arguments) {
        int playerId = Integer.parseInt(arguments);
        screen.removePlayer(playerId);
    }

}
