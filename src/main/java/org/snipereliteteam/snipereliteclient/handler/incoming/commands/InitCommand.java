package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

public class InitCommand extends AbstractCommand {

    @Override
    public void execute(String arguments) {
        screen.initArena();
    }

}
