package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

import org.snipereliteteam.snipereliteclient.screen.Screen;

public abstract class AbstractCommand implements ResponseCommand {

    protected Screen screen;


    @Override
    public void setScreen(Screen screen) {
        this.screen = screen;
    }

}
