package org.snipereliteteam.snipereliteclient.handler.incoming.commands;

public enum ResponseCommandsEnum {

    ARENA(new ArenaCommand()),
    MOVE(new MoveCommand()),
    ADD(new AddCommand()),
    ID(new IdCommand()),
    INIT(new InitCommand()),
    SHOT(new ShotCommand()),
    SHOT_R(new ShotRemoveCommand()),
    KILL(new KillCommand()),

    INVALID(new InvalidCommand());

    private final ResponseCommand command;


    ResponseCommandsEnum(ResponseCommand command) {
        this.command = command;
    }


    public ResponseCommand getCommand() {
        return command;
    }

}
