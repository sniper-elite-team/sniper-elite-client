package org.snipereliteteam.snipereliteclient.handler.outgoing;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class RequestHandler {

    public static void sendKeyPressed(Socket socket, KeyboardEvent event) {

        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream());

            if (event.getKey() == KeyboardEvent.KEY_SPACE) {
                out.println("SHOT#");
            } else {
                out.print("MOVE#");
            }
            switch (event.getKey()) {
                case KeyboardEvent.KEY_UP:
                    out.println("UP");
                    break;
                case KeyboardEvent.KEY_DOWN:
                    out.println("DOWN");
                    break;
                case KeyboardEvent.KEY_LEFT:
                    out.println("LEFT");
                    break;
                case KeyboardEvent.KEY_RIGHT:
                    out.println("RIGHT");
            }
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
