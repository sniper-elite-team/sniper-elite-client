package org.snipereliteteam.snipereliteclient.listener;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.snipereliteteam.snipereliteclient.handler.outgoing.RequestHandler;
import org.snipereliteteam.snipereliteclient.screen.Screen;

import java.net.Socket;

public class KeyboardListener implements KeyboardHandler {

    private final Screen screen;
    private final Socket socket;

    private static int[] KEY_CODES = {
            KeyboardEvent.KEY_SPACE,
            KeyboardEvent.KEY_UP,
            KeyboardEvent.KEY_DOWN,
            KeyboardEvent.KEY_LEFT,
            KeyboardEvent.KEY_RIGHT,
    };


    public KeyboardListener(Screen screen, Socket socket) {
        this.screen = screen;
        this.socket = socket;
        init();
    }


    public void init() {
        Keyboard keyboard = new Keyboard(this);

        for (int code : KEY_CODES) {
            subscribe(keyboard, code);
        }
    }


    private void subscribe(Keyboard keyboard, int code) {
        KeyboardEvent event = new KeyboardEvent();
        event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        event.setKey(code);
        keyboard.addEventListener(event);
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        RequestHandler.sendKeyPressed(socket, keyboardEvent);
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

}
