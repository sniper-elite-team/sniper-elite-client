package org.snipereliteteam.snipereliteclient;

public class Main {

    public static void main(String[] args) {

        String ipAddress = "2.82.171.4"; //127.0.0.1
        int portNumber = 25566;

        Client client = new Client();
        client.init(ipAddress, portNumber);

    }

}
