package org.snipereliteteam.snipereliteclient;

public class Config {

    public static final int SCREEN_HEIGHT = 600;
    public static final int SCREEN_WIDTH = 800;

    public static final int PLAYER_SIZE = 62;

}
