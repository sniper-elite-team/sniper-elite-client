package org.snipereliteteam.snipereliteclient;

import org.snipereliteteam.snipereliteclient.handler.incoming.ResponseHandler;
import org.snipereliteteam.snipereliteclient.handler.incoming.commands.ResponseCommandsEnum;
import org.snipereliteteam.snipereliteclient.listener.KeyboardListener;
import org.snipereliteteam.snipereliteclient.listener.MouseListener;
import org.snipereliteteam.snipereliteclient.screen.Screen;

import java.io.IOException;
import java.net.Socket;

public class Client {

    private Socket socket;


    public void init(String ipAddress, int portNumber) {

        try {
            socket = new Socket(ipAddress, portNumber);
        } catch (IOException e) {
            System.err.println("Can't reach the server!");
            // TODO: 30/01/2021 Add tries and timeouts
            return;
        }

        bootstrap();
    }


    private void bootstrap() {

        Screen screen = new Screen();
        screen.init();

        MouseListener mouseListener = new MouseListener(screen, socket);

        KeyboardListener keyboardListener = new KeyboardListener(screen, socket);

        for (ResponseCommandsEnum command : ResponseCommandsEnum.values()) {
            command.getCommand().setScreen(screen);
        }

        ResponseHandler responseHandler = new ResponseHandler(socket);
        responseHandler.startListening();
    }


    public void closeSocket() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
