package org.snipereliteteam.snipereliteclient.screen;

import org.snipereliteteam.snipereliteclient.game_object.GameObject;
import org.snipereliteteam.snipereliteclient.game_object.Projectile;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ObjectThread implements Runnable {

    private final List<GameObject> gameObjects = new CopyOnWriteArrayList<>();

    private final Screen screen;


    public ObjectThread(Screen screen) {
        this.screen = screen;
    }


    @Override
    public void run() {
        while (screen.isInGame()) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                // Nothing to handle
            }

            for (GameObject gameObject : gameObjects) {
                switch (((Projectile) gameObject).getOrientation()) {
                    case "N":
                        gameObject.translate(new Vector2D(0, -25));
                        break;
                    case "S":
                        gameObject.translate(new Vector2D(0, 25));
                        break;
                    case "W":
                        gameObject.translate(new Vector2D(-25, 0));
                        break;
                    case "E":
                        gameObject.translate(new Vector2D(25, 0));
                }
            }

        }
    }


    public void addObject(GameObject gameObject) {
        gameObjects.add(gameObject);
    }


    public <T extends GameObject> void removeObject(int id, Class<T> objectClass) {

        GameObject gameObject = gameObjects.stream()
                .filter(objectClass::isInstance)
                .filter(x -> x.getId() == id)
                .findFirst().get();

        gameObject.getImage().delete();
        gameObjects.remove(gameObject);
    }

}
