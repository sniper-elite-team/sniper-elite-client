package org.snipereliteteam.snipereliteclient.screen;

import org.academiadecodigo.simplegraphics.graphics.Canvas;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.snipereliteteam.snipereliteclient.Config;
import org.snipereliteteam.snipereliteclient.game_object.Player;
import org.snipereliteteam.snipereliteclient.game_object.Position;
import org.snipereliteteam.snipereliteclient.game_object.Projectile;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Screen {

    private Picture background;
    private final Map<Integer, Player> players = new HashMap<>();

    private int myPlayerId;

    private final ExecutorService service = Executors.newSingleThreadExecutor();
    private final ObjectThread objectThread = new ObjectThread(this);


    public void init() {

        Canvas canvas = Canvas.getInstance();

        canvas.getFrame().setSize(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT);
        Rectangle back = new Rectangle(0, 0, Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT);
        back.setColor(Color.BLACK);
        back.fill();
    }


    public void setMyPlayerId(int myPlayerId) {
        this.myPlayerId = myPlayerId;
    }


    public void buildArena(Picture background) {
        this.background = background;
    }


    public void addPlayer(int id, Player player) {
        players.put(id, player);
    }


    public void initArena() {

        service.execute(objectThread);

        int vX = players.get(myPlayerId).getPosition().getX() - (Config.SCREEN_WIDTH / 2) + (Config.PLAYER_SIZE / 2);
        int vY = players.get(myPlayerId).getPosition().getY() - (Config.SCREEN_HEIGHT / 2) + (Config.PLAYER_SIZE / 2);

        Vector2D vector = new Vector2D(-vX, -vY);

        background.translate(-vX, -vY);

        for (Integer id : players.keySet()) {
            players.get(id).translate(vector);
        }

        background.draw();
        for (Player player : players.values()) {
            player.getImage().draw();
        }

    }


    public void movePlayers(int id, Position playerPositionMap, String orientation) {

        int vx = playerPositionMap.getX() - ((-background.getX()) + players.get(id).getPosition().getX());
        int vy = playerPositionMap.getY() - ((-background.getY()) + players.get(id).getPosition().getY());

        Vector2D vector = new Vector2D(-vx, -vy);

        rotatePlayer(players.get(id), orientation);
        if (id == myPlayerId) {
            moveBackground(id, vector);
            for (Player player : players.values()) {
                if (player.getId() == myPlayerId) {
                    continue;
                }
                player.translate(vector);
            }
            return;
        }
        players.get(id).translate(new Vector2D(vx, vy));
    }


    public void removePlayer(int playerId) {
        if (playerId == myPlayerId) {
            System.exit(0);
        }
        players.get(playerId).getImage().delete();
        players.remove(playerId);

    }


    public void addProjectile(int id, Position projectilePositionMap, String orientation) {

        int x = projectilePositionMap.getX() - (-background.getX());
        int y = projectilePositionMap.getY() - (-background.getY());

        Picture image = new Picture(x, y, "projectile" + orientation + ".png");
        Position position = new Position(x, y);
        Projectile projectile = new Projectile(image, position, id, orientation);
        projectile.getImage().draw();

        objectThread.addObject(projectile);
    }


    public void removeProjectile(int projectileId) {
        objectThread.removeObject(projectileId, Projectile.class);
    }


    public boolean isInGame() {
        return true;
    }


    public void moveBackground(int id, Vector2D vector2D) {
        if (id != myPlayerId) {
            return;
        }
        background.translate(vector2D.getX(), vector2D.getY());
    }


    private void rotatePlayer(Player player, String orientation) {
        player.getImage().load("sniper" + orientation + ".png");
    }

}
