package org.snipereliteteam.snipereliteclient.screen;

public class Vector2D {

    private final int x;

    private final int y;


    public Vector2D(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }

}
